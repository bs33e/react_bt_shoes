import React, { Component } from "react";

export default class CartShoes extends Component {
  
  renderTbody = () => {
    
    return this.props.cart.map((item, index) => {
      
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-dark"
            >
              +
            </button>
            <span className="px-4">{item.soLuong}</span>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-dark"
            >
              -
            </button>
          </td>
          <td>
            <img width={50} src={item.image} alt="" />
          </td>
          <td>
            <button onClick={() => {this.props.handleDeleteItem(item.id)}} className="btn btn-light">
              <i class="fa-solid fa-square-minus"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Your cart</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">x</span>
              </button>
            </div>
            <div className="modal-body">
              {" "}
              <table className="table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Picture</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>{this.renderTbody()}</tbody>
                <tfoot>
                  <h4>
                    Total:
                    <span className="ml-3">
                      {this.props.cart.reduce((total, cart, index) => {
                        return (total += cart.soLuong * cart.price);
                      }, 0)}
                    </span>
                  </h4>
                </tfoot>
              </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                By now
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
