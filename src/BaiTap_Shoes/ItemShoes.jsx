import React, { Component } from "react";

export default class ItemShoes extends Component {
  render() {
    let {image, name} = this.props.data;
    
    return (
      <div className="col-3 my-3">
        <div className="card h-100">
          <img className="card-img-top w-100" src={image} alt />
          <div className="card-body bg-dark text-light text-left">
            <h4 className="card-title">{name}</h4>
            <button onClick={() => {
                this.props.handleAddToCart(this.props.data)
            }} className="btn btn-light">Add to cart</button>
          </div>
        </div>
      </div>
    );
  }
}
