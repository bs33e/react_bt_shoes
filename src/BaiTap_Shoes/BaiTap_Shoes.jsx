import React, { Component } from "react";
import CartShoes from "./CartShoes";
import { data_shoes } from "./dataShoes";
import ItemShoes from "./ItemShoes";

export default class BaiTap_Shoes extends Component {
  state = {
    shoes: data_shoes,
    cart: [],
  };

  renderContent = () => {
    return this.state.shoes.map((item, index) => {
      return (
        <ItemShoes
          key={index}
          data={item}
          handleAddToCart={this.handleAddToCart}
        />
      );
    });
  };
  handleAddToCart = (shoe) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    let cloneCart = [...this.state.cart];

    // th1 trong cart chưa có sản phẩm
    if (index == -1) {
      let newItem = { ...shoe, soLuong: 1 };
      cloneCart.push(newItem);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({cart: cloneCart});

    //th2 trong cart đã có sản phẩm
  };
  handleChangeQuantity = (idShoe, value) => {
    let index = this.state.cart.findIndex((shoe) => {
        return shoe.id == idShoe;
    });
    if (index == -1) return;
   
        let cloneCart = [...this.state.cart];
        cloneCart[index].soLuong = cloneCart[index].soLuong + value;
        cloneCart[index].soLuong ==0 && cloneCart.splice(index,1);
        this.setState({ cart: cloneCart });
  };

  handleDeleteItem =(idShoe) => {

    let index = this.state.cart.findIndex((shoe) => {
      return shoe.id == idShoe;
    });
    let cloneCart = [...this.state.cart];
    cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
   

    // this.setState({
    //   cart: cartUpdate
    // })
  }

  render() {
    let totalQty = this.state.cart.reduce((total, item, index) => {
      return total += item.soLuong;
    }, 0)
    return (
      <div className="container py-5">
        <h2 className="bg-dark text-white py-3">SHOES SHOP</h2>
        <div className="text-right">

        <button width={400}  className="btn btn-dark text-white"  data-toggle="modal" data-target="#modelId"><i  class="fa-solid fa-cart-shopping ">( {totalQty} )</i></button>
        </div>
        
        <div className="row">{this.renderContent()}</div>

        
              <CartShoes 
                handleDeleteItem = {this.handleDeleteItem}
                handleChangeQuantity={this.handleChangeQuantity}
                cart={this.state.cart} />
      </div>
    );
  }
}

